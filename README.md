# Epidemic Simulator

## Objective

Simulate the effect of existing measures: stay home, close school, shut down.

Please check out the `html` file. The code and the result are inside `epidemic_simulator.html`.
